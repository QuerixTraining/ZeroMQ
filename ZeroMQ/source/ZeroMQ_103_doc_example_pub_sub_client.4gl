##########################################################################
# ZeroMQ Project                                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

	DEFINE context ZMQ.Context
	DEFINE subscriber ZMQ.Socket
	DEFINE s STRING
	DEFINE updateNbr, totalTemp, avgTemp INT
	DEFINE zipcode, temperature, relhumidity INT
	DEFINE filter STRING

    # Socket to talk to server
	DISPLAY "Collecting updates from weather server"
	LET subscriber = context.Socket("ZMQ.SUB")
	CALL subscriber.connect("tcp://localhost:5556")
    
    # Subscribe to zipcode, default is NYC, 10001
	LET filter = "10001 "
	CALL subscriber.subscribe(filter)
 
    # Process 100 updates
    LET totalTemp = 0
    FOR updateNbr = 0 TO 100
        LET s = subscriber.recv()
        DISPLAY s
		CALL split(s) RETURNING zipcode, temperature, relhumidity
		LET totalTemp = totalTemp + temperature
	END FOR
    
    LET avgTemp = totalTemp/updateNbr
    
	DISPLAY "Average temperature for zipcode "||zipcode||" was "||avgTemp
			
	CALL subscriber.close()	
	CALL context.term()
	
END MAIN

FUNCTION split(str)

DEFINE str STRING 
DEFINE zip, tmpr, hum INT
DEFINE ind, pos1, pos2 INT

LET pos1 = str.getIndexOf(" ",1)
LET pos2 = str.getIndexOf(" ",pos1+1)
LET zip = str.subString(1,pos1-1)
LET tmpr = str.subString(pos1+1,pos2-1)
LET hum = str.subString(pos2+1,str.getLength())

RETURN zip, tmpr, hum

END FUNCTION 


