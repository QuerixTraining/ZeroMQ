##########################################################################
# ZeroMQ Project                                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
	DEFINE context ZMQ.Context
	DEFINE receiver ZMQ.Socket
	DEFINE s STRING
	DEFINE task_nbr, total_sec INT
	DEFINE tstart, tend DATETIME HOUR TO FRACTION
	DEFINE res INTERVAL SECOND TO SECOND
	
	# Prepare our context and socket
	LET receiver = context.Socket("ZMQ.PULL")
	CALL receiver.bind("tcp://*:5556")
    # Wait for start of batch
	LET s = receiver.recv()
	
	# Start our clock now
	LET tstart = CURRENT
	
	LET total_sec = 0 # Total calculated cost in secs
	# Process 100 confirmations
	FOR task_nbr = 0 TO 50
   		LET s = receiver.recv()
		IF (task_nbr / 10) * 10 == task_nbr THEN
			DISPLAY ":",task_nbr
		ELSE 
			DISPLAY "."
		END IF
	END FOR
    # Calculate and report duration of batch
	LET tend = CURRENT
	LET res = tend - tstart
	DISPLAY "Total elapsed time: "||res||" sec"
	
	CALL receiver.close()
	CALL context.term()
END MAIN