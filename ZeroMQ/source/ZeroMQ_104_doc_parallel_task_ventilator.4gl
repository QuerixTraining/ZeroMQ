##########################################################################
# ZeroMQ Project                                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
	DEFINE context ZMQ.Context
	DEFINE sink, sender ZMQ.Socket
	DEFINE s STRING
	DEFINE yes CHAR
	DEFINE srandom, total_sec, workload INT
	DEFINE task_nbr INT
    
    # Socket to send messages on
	LET sender = context.Socket("ZMQ.PUSH")
	CALL sender.bind("tcp://*:5557")
    # Socket to send messages on
	LET sink = context.Socket("ZMQ.PUSH")
	CALL sink.connect("tcp://localhost:5556")

	
	PROMPT "Press 'Y' when the workers are ready: " FOR CHAR yes
	IF yes MATCHES "[Yy]" THEN
		DISPLAY "Sending tasks to workers\n"
		# The first message is "0" and signals start of batch
		CALL sink.send("0")
		LET total_sec = 0
		FOR task_nbr = 0 TO 50
			LET srandom = fgl_random(0, 5)
	    	LET workload = srandom + 1
     		LET total_sec = total_sec + workload
     		DISPLAY workload||"."
     		LET s = workload
	    	CALL sender.send(s)
		END FOR
	
		DISPLAY "Total expected cost: ", total_sec, " sec"
		SLEEP 1
	ELSE
		DISPLAY "Program wasn't started :( "
	END IF		 
	CALL sink.close()
	CALL sender.close()
	CALL context.term()
END MAIN