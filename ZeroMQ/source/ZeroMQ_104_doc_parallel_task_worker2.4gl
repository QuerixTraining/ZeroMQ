##########################################################################
# ZeroMQ Project                                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
	DEFINE context ZMQ.Context
	DEFINE receiver, sender ZMQ.Socket
	DEFINE s STRING
	DEFINE sec INT

    # Socket to receive messages on
	LET receiver = context.Socket("ZMQ.PULL")
	CALL receiver.connect("tcp://localhost:5557")
    # Socket to send messages to
	LET sender = context.Socket("ZMQ.PUSH")
	CALL sender.connect("tcp://localhost:5556")
	
	# Process tasks forever
	WHILE TRUE
    	LET s = receiver.recv()
    	LET sec = s
    	# Simple progress indicator for the viewer
		DISPLAY s||"."
		# Do the work
		SLEEP sec
		# Send results to sink
		CALL sender.send("")
	END WHILE
	CALL receiver.close()
	CALL sender.close()
	CALL context.term()
END MAIN