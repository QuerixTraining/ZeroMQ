##########################################################################
# ZeroMQ Project                                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

# Project: build_in_packages
# Filename: ZeroMQ_102_doc_example_reporting_version.4gl
# Created By: egza
# Creation Date: May 13, 2016

MAIN

	DEFINE version STRING

	LET version = ZMQ.Utils.GetVersion()
	DISPLAY "Current version is: ",version		
		
END MAIN

