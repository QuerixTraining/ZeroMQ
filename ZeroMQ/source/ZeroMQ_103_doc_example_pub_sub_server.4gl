##########################################################################
# ZeroMQ Project                                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
    
	DEFINE context ZMQ.Context
	DEFINE publisher ZMQ.Socket
	DEFINE s_update STRING
	DEFINE zipcode, temperature, relhumidity INT 
	DEFINE i INT
	
	# Prepare our context and publisher
	LET publisher = context.Socket("ZMQ.PUB")
	CALL publisher.bind("tcp://*:5556")
	WHILE TRUE
    	#  Get random values for the weather server
    	LET zipcode = 10000 + fgl_random(1, 1000)
    	LET temperature = fgl_random(-40, 40)
    	LET relhumidity = fgl_random(0, 100)
		 
    	LET s_update = zipcode||" "||temperature||" "||relhumidity
    	#  Send message to all subscribers
    	CALL publisher.send(s_update)
	END WHILE
	CALL publisher.close()
	CALL context.term()

END MAIN