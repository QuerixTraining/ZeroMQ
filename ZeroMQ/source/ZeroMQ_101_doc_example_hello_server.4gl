##########################################################################
# ZeroMQ Project                                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

# Project: build_in_packages
# Filename: ZeroMQ_101_doc_example_hello_server.4gl
# Created By: egza
# Creation Date: May 13, 2016


#
#  Hello World server in 4GL
#  Binds REP socket to tcp://*:5556
#  Expects "Hello" from client, replies with "World"
#

MAIN
    
	DEFINE context ZMQ.Context
	DEFINE responder ZMQ.Socket
	DEFINE request, reply STRING
	
	# Socket to talk to clients
 	LET responder = context.Socket("ZMQ.REP")
 	CALL responder.bind("tcp://*:5556")
	
 	WHILE TRUE
   		# Wait for next request from the client
   		LET request = responder.recv()
   		DISPLAY "Received Hello"
   		
   		# Do some 'work'
   		SLEEP 1
   		
   		# Send reply back to client
   		LET reply = "World!"
   		CALL responder.send(reply)
 	END WHILE
 	
 	CALL responder.close()
 	CALL context.term()

END MAIN