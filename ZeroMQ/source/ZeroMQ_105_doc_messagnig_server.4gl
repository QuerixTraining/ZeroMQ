##########################################################################
# ZeroMQ Project                                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
	DEFINE context ZMQ.Context
	DEFINE reply ZMQ.Socket
 	DEFINE msg ZMQ.Msg
	DEFINE cust_rec RECORD
	    	cust_num INTEGER,
			cust_name VARCHAR(30),
			order_ids DYNAMIC ARRAY OF INTEGER
		   END RECORD
    # Socket to talk to clients
 	LET reply = context.Socket("ZMQ.REP")
 	CALL reply.bind("tcp://*:5556")
	
	# Receiving message from client
	LET msg = reply.recv()
	CALL msg.get(cust_rec)
	
	DISPLAY cust_rec	
		
	CALL reply.close() 
	CALL context.term()
END MAIN