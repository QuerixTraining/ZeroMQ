##########################################################################
# ZeroMQ Project                                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
	DEFINE context ZMQ.Context
	DEFINE request ZMQ.Socket
 	DEFINE msg ZMQ.Msg
	DEFINE cust_rec RECORD
           cust_num INTEGER,
           cust_name VARCHAR(30),
           order_ids DYNAMIC ARRAY OF INTEGER
           END RECORD
	
	# Socket to talk to server
	LET request = context.Socket("ZMQ.REQ")
	CALL request.connect("tcp://localhost:5556")
	
	# Filling 4GL RECORD with values
    LET cust_rec.cust_num = 345
    LET cust_rec.cust_name = "McMaclum"
    LET cust_rec.order_ids[1] = 4732
    LET cust_rec.order_ids[2] = 9834
    LET cust_rec.order_ids[3] = 2194
	
	# Initializing message from 4GL RECORD
	CALL msg.init(cust_rec)
	# Sending message
	CALL request.send(msg)

	CALL request.close()
	CALL context.term()
END MAIN