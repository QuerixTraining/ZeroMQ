##########################################################################
# ZeroMQ Project                                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

# Project: build_in_packages
# Filename: ZeroMQ_101_doc_example_hello_client.4gl
# Created By: egza
# Creation Date: May 13, 2016


#
# Hello World client in 4GL
# Connects REQ socket to tcp://localhost:5556
# Sends "Hello" to server, expects "World" back
#

MAIN

	# Defining variables	
	DEFINE context ZMQ.Context
	DEFINE requester ZMQ.Socket
	DEFINE request, reply STRING
	DEFINE requestNbr INT
	
	#  Socket to talk to server
	DISPLAY "Connecting to hello world server..."
	
	LET requester = context.Socket("ZMQ.REQ")
	CALL requester.connect("tcp://localhost:5556")
	
	FOR requestNbr = 0 TO 10
		LET request = "Hello"
		DISPLAY "Sending Hello ", requestNbr
		CALL requester.send(request)
		
		LET reply = requester.recv()
		DISPLAY "Received ", reply CLIPPED, " ", requestNbr
	END FOR

	CALL requester.Close()
	CALL context.term()	

END MAIN
